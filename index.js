console.log("Welcome to Pokemon World");

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	talk: function(){
		console.log("Pikachu! I choose you!");
	}

}
console.log(trainer);
console.log(trainer.name);
console.log(trainer.pokemon);
trainer.talk();

function Pokemon(name,level){
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = 1.5 * level;
	this.tackle = function(pokemon){
		console.log(this.name + " tackled " + pokemon.name);
		pokemon.health = pokemon.health - this.attack
		console.log(pokemon.name + "'s health is now reduced to " + pokemon.health)
	}
	this.faint = function(){
		if (pokemon.health <= 0) {
			console.log(pokemon.name + " has fainted!")
		}

	}
	
}

let pokemon1 = new Pokemon("Pikachu",12);
let pokemon2 = new Pokemon("Geodude",8);
let pokemon3 = new Pokemon("Mewtwo",100);
console.log(pokemon1);
console.log(pokemon2);
console.log(pokemon3);

pokemon1.tackle(pokemon2);
pokemon3.tackle(pokemon1);